var Megamenu_framework_persistant_state = {index: -1, active: 0, parse_focus: 1};
(function ($) {
  Drupal.behaviors.megamenu_framework = {
    attach: function (context, settings) {
      // BEGIN document ready

      // Hover actions on the main menu items.
      $('#block-megamenu-framework-megamenu-framework div.mainmenu-item').mouseover(function (e) {
        $(this).addClass('hover');
      });
      $('#block-megamenu-framework-megamenu-framework div.mainmenu-item').mouseout(function (e) {
        $(this).removeClass('hover');
      });
      // If links are turned on and a category has no url assignment, prevent the click from doing anything.
      $('#block-megamenu-framework-megamenu-framework div.mainmenu-item a[href="#"]').click(function(event){
        event.preventDefault();
      });

      if (Drupal.settings.megamenu_framework != undefined) {
        if (Drupal.settings.megamenu_framework.eventType == 'click') {
          // Activation of menu pulldowns (click event)
          $('#block-megamenu-framework-megamenu-framework div.mainmenu-item').click(function (e) {

            var megamenu_items = $('#block-megamenu-framework-megamenu-framework div.mainmenu-item');
            //Get the clicked index
            var click_index = megamenu_items.index($(this));

            // Clean up previous states
            $('#block-megamenu-framework-megamenu-framework .megamenu-pulldown-wrapper').not('#block-megamenu-framework-megamenu-framework .megamenu-pulldown-wrapper:eq(' + click_index + ')').hide();
            megamenu_items.removeClass('active');

            /*
             Activate the pulldown
             jQuery's hasClass() method does not work on dynamically created classes, so toggling the "active" class is not so simple
             We have to store the state of this class in a persistent variable (Global variable declared at the top of this script.
             We start by capturing the index of the clicked menu item
             */
            var target_megamenu_item = $(this);
            var megamenu_active_index = megamenu_items.index(target_megamenu_item);

            /*
             We store the index and the active state in that variable on click events
             And test it against the clicked item to determine if we're adding or removing the active state.
             If the stored index and the clicked index match, then we're clicking on an active button (thereby toggling it off).
             Therefore we remove the "active" class
             Otherwise we add the class.

             Toggle the visibility of the pulldown
             We don't use the toggle() method here. It is not very reliable in this case.
             */
            if (Megamenu_framework_persistant_state.index == megamenu_active_index) {
              $(this).removeClass('active');
              Megamenu_framework_persistant_state.active = 0; // off
              Megamenu_framework_persistant_state.index = -1; // no index
              Megamenu_framework_persistant_state.parse_focus = 0; // Disable the processing of focus events. See comment in the focus() handler
              $('#block-megamenu-framework-megamenu-framework .megamenu-pulldown-wrapper:eq(' + megamenu_active_index + ')').hide();
            }
            else {
              $(this).addClass('active');
              Megamenu_framework_persistant_state.active = 1; // On
              Megamenu_framework_persistant_state.index = megamenu_active_index; // index of clicked object
              Megamenu_framework_persistant_state.parse_focus = 1; // Enable the processing of focus events. See comment in the focus() handler
              $('#block-megamenu-framework-megamenu-framework .megamenu-pulldown-wrapper:eq(' + megamenu_active_index + ')').show();
            }

            // None of this works at all without this call.
            e.stopPropagation();
          });

          // Clicking ANYWHERE, other than the main menu items closes the menu.
          $('html').click(function () {
            $('#block-megamenu-framework-megamenu-framework .megamenu-pulldown-wrapper').hide();
            $('#block-megamenu-framework-megamenu-framework div.mainmenu-item').removeClass('active');
            Megamenu_framework_persistant_state.active = 0;
            Megamenu_framework_persistant_state.index = -1;
          });

          // Exceptions to the above operation. jQuery selectors for this are entered in the module's ui.
          if (Drupal.settings.megamenu_framework.stopPropagations != undefined) {
            $(Drupal.settings.megamenu_framework.stopPropagations).click(function (e) {
              e.stopPropagation();
            });
          }

          // Tabbing reactions. For 508 compliance
          $('#block-megamenu-framework-megamenu-framework div.mainmenu-item').focus(function(){
            // The parse focus variable property is to prevent some un-expected behavior.
            // If you click off a menu then leave the browser and then return to the browser window, the menu pops up again.
            // Because it retains the focus from before and the focus is re-processed when the browser window goes active again.
            // So this property is always true, except when closing a menu with a click, when it's set to false.
            if (Megamenu_framework_persistant_state.parse_focus) {
              var megamenu_items = $('#block-megamenu-framework-megamenu-framework div.mainmenu-item');
              var focus_index = megamenu_items.index($(this));

              // Initialize by closing all the pulldowns
              $('#block-megamenu-framework-megamenu-framework .megamenu-pulldown-wrapper').hide();

              // Activate the target pulldown
              var openPulldownTimeoutID = setTimeout((function (focus_index) {
                return function () {
                  openPulldown(focus_index);
                };
              })(focus_index), 100);
            }

          });
        }
        else {
          // Activate pulldowns on hover event
          if (Drupal.settings.megamenu_framework.linksOn && Drupal.settings.megamenu_framework.linksOn == 1) {
            var trigger_obj = '#block-megamenu-framework-megamenu-framework div.mainmenu-item a';
          }
          else {
            var trigger_obj = '#block-megamenu-framework-megamenu-framework div.mainmenu-item';
          }
          $(trigger_obj).hover(
            function (e) {
              var megamenu_items = $('#block-megamenu-framework-megamenu-framework div.mainmenu-item');

              //Get the hovered index
              if (Drupal.settings.megamenu_framework.linksOn && Drupal.settings.megamenu_framework.linksOn == 1) {
                var hover_index = megamenu_items.index($(this).parent().parent());
              }
              else {
                var hover_index = megamenu_items.index($(this));
              }

              // Initialize by closing all the pulldowns
              $('#block-megamenu-framework-megamenu-framework .megamenu-pulldown-wrapper').hide();

              // Activate the target pulldown
              // This complex timeout construct is for compatibility in IE and Firefox
              // See: http://stackoverflow.com/questions/9568248/settimeout-internet-explorer#9568281
              var openPulldownTimeoutID = setTimeout((function (hover_index) {
                return function () {
                  openPulldown(hover_index);
                };
              })(hover_index), 100);

            }, function (e) {
              /*
               Close when rolling off of a menu item
               We pass in a jquery id for any pulldown hover event.
               The prevents the pulldown from closing when we roll from a menu item to its pulldown.
               */
              var pulldownHover = '#block-megamenu-framework-megamenu-framework .megamenu-pulldown-wrapper:hover';
              var closePulldownTimeoutID = setTimeout((function (pulldownHover) {
                return function () {
                  closePulldown(pulldownHover);
                };
              })(pulldownHover), 100);

            }
          );

          // Close when rolling off the pulldown.
          $('#block-megamenu-framework-megamenu-framework .megamenu-pulldown-wrapper').mouseleave(function (e) {
            var closePulldownTimeoutID = setTimeout((function () {
              return function () {
                closePulldown();
              };
            })(), 100);
          });

          // React to tab events, for 508 compliance
          $(trigger_obj).focus(function(){
            var megamenu_items = $('#block-megamenu-framework-megamenu-framework div.mainmenu-item');

            //Get the hovered index
            if (Drupal.settings.megamenu_framework.linksOn && Drupal.settings.megamenu_framework.linksOn == 1) {
              var focus_index = megamenu_items.index($(this).parent().parent());
            }
            else {
              var focus_index = megamenu_items.index($(this));
            }

            // Initialize by closing all the pulldowns
            $('#block-megamenu-framework-megamenu-framework .megamenu-pulldown-wrapper').hide();

            // Activate the target pulldown
            var openPulldownTimeoutID = setTimeout((function (focus_index) {
              return function () {
                openPulldown(focus_index);
              };
            })(focus_index), 100);
          });

        }




        $('#block-megamenu-framework-megamenu-framework .megamenu-pulldown-wrapper.last').focusout(function(){
          $('*').focus(function() {
              if ($(this).parents('#block-megamenu-framework-megamenu-framework .megamenu-pulldown-wrapper.last').length == 0) {
                $('#block-megamenu-framework-megamenu-framework .megamenu-pulldown-wrapper:last').hide();
              }
          });
        });

      }

      /**
       A plugin for setting active-trail class on a main menu item.
       Arg: index = the 0 based index of the main menu item you want to set.
       Usage: $(document).megamenu_framework_set_active_trail(index);

       Calculating the index is beyond the scope of this module. You'll have to do that yourself.
       */
      $.fn.megamenu_framework_set_active_trail = function (index) {
        $('#block-megamenu-framework-megamenu-framework div.mainmenu-item:eq(' + index + ')').each(function () {
          $(this).addClass('active-trail');
        });
      };

      function openPulldown(index) {
        $('#block-megamenu-framework-megamenu-framework .megamenu-pulldown-wrapper:eq(' + index + ')').show();
      }

      function togglePulldown(index) {
        $('#block-megamenu-framework-megamenu-framework .megamenu-pulldown-wrapper:eq(' + index + ')').toggle();
      }

      /**
       @param jqueryTestString (optional)
       We pass in a jquery ID string to test for a hover state on ANY pulldown.
       We only close the pulldown if that tests false. This prevents the popup from closing when we roll off a main menu item
       onto its pulldown.

       If we don't pass in an argument, it closes all pulldowns by default. This gets called when we roll off a pulldown
       */
      function closePulldown(jqueryTestString) {
        if (jqueryTestString != undefined) {
          if (!$(jqueryTestString).length) {
            // Rolling off of menu item: close all pulldowns
            // Rolling onto another will open a new one
            $('#block-megamenu-framework-megamenu-framework .megamenu-pulldown-wrapper').hide();
          }
        }
        else {
          // Rolling off of a pulldown: close all pulldowns
          $('#block-megamenu-framework-megamenu-framework .megamenu-pulldown-wrapper').hide();
        }
      }

      // END document ready
    }
  };
})(jQuery);
