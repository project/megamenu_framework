<?php
/**
 * @file
 * Controls the admin interface of this module.
 */

/*
 * Combines forms into a single admin form
 */
function megamenu_framework_menuitems_admin() {
  drupal_add_js(drupal_get_path('module', 'megamenu_framework') . '/inc/megamenu_framework_admin.js', array('type' => 'file', 'preprocess' => FALSE));
  $form['second_form'] = drupal_get_form('megamenu_framework_menuitems_admin_form');
  $form['first_form'] = drupal_get_form('megamenu_framework_admin_form');
  $form['third_form'] = drupal_get_form('megamenu_framework_stop_propegation_obj_form');
  return $form;
}


/*
 * Implements hook_form().
 */
function megamenu_framework_admin_form($form, &$form_state) {
  if (module_exists('context')) {
    $form['placement'] = array(
        '#type' => 'radios',
        '#required' => FALSE,
        '#title' => t('Block Placement'),
        '#default_value' => variable_get('megamenu_framework_placement', ''),
        '#options' => array('block_first' => t('Core Block UI first'), 'context_first' => t('Context module first')),
        '#description' => t('This module supports placing block content with both the Core Block UI and the Context Module. While both methods can be used at the same time, their content cannot be intermingled. So choose with one you want to appear at the top of each region.'),
    );
  }
  $form['event_type'] = array(
      '#type' => 'radios',
      '#required' => TRUE,
      '#title' => t('Triggering Event'),
      '#default_value' => variable_get('megamenu_framework_event_type', ''),
      '#options' => array('hover' => t('Hover'), 'click' => t('Click')),
      '#description' => t('Select the type of event that triggers the pulldowns.'),
  );
  $form['links'] = array(
    '#type' => 'checkbox',
    '#required' => FALSE,
    '#title' => t('Links on Hover elements'),
    '#default_value' => variable_get('megamenu_framework_links', 0),
    '#description' => t('Turn on links on the main menu items. If you turn this on, all main category items will have links. If they don\'t have a url assigned to them the links will have an href="#" and a class "empty"'),
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Placement and Event'),
  );
  return $form;
}


/*
 * Implements hook_form_submit().
 */
function megamenu_framework_admin_form_submit($form, $form_state) {
  variable_set('megamenu_framework_event_type', $form_state['values']['event_type']);
  if (module_exists('context')) {
    variable_set('megamenu_framework_placement', $form_state['values']['placement']);
  }
  variable_set('megamenu_framework_links', $form_state['values']['links']);
  drupal_set_message(t('Your settings have been saved.'));
}


/*
 * Implements hook_form().
 */
function megamenu_framework_menuitems_admin_form($form, &$form_state) {
  $result = db_select('megamenu_framework', 'mf')->fields('mf', array('rid', 'title', 'machine_name', 'columns', 'weight'))->orderBy('mf.weight')->execute()->fetchAll();

  $form['#tree'] = TRUE;
  $form['#theme'] = 'megamenu_framework_menuitems_admin_form';
  $form['help'] = array(
    '#type' => 'markup',
    '#markup' => 'Categories for the megamenu. Each category will generate a main menu item and a matching megamenu pulldown. Specify the number of columns each pulldown will have. The theme registry will be flushed every time to add, edit or delete a category',
  );
  foreach ($result as $row) {
    $form['categories'][$row->rid]['title'] = array(
      '#markup' => $row->title,
    );
    $form['categories'][$row->rid]['machine_name'] = array(
      '#markup' => $row->machine_name,
    );
    $form['categories'][$row->rid]['columns'] = array(
      '#markup' => $row->columns,
    );
    $form['categories'][$row->rid]['ops'] = array(
      '#markup' => l(t('Edit'), MEGAMENU_FRAMEWORK_ADMINPATH . '/edit/' . $row->rid) . ' | ' . l(t('Delete'), MEGAMENU_FRAMEWORK_ADMINPATH . '/delete/' . $row->rid),
    );
    $form['categories'][$row->rid]['weight'] = array(
      '#type' => 'weight',
      '#title' => t('Weight for @title', array('@title' => $row->rid)),
      '#title_display' => 'invisible',
      '#default_value' => $row->weight,
    );
    $form['categories'][$row->rid]['rid'] = array(
      '#type' => 'hidden',
      '#value' => $row->rid,
    );
  }

  $form['actions'] = array('#type' => 'actions');
  if (count($result) > 1) {
    $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Save Changes to Ordering'));
  }
  elseif (count($result) < 1) {
    $form['no_paths_records'] = array(
      '#type' => 'markup',
      '#markup' => '<div><br />' . t('<strong>There are no megamenu categories configured. Start by adding a category</strong>') . '</div>',
    );
  }
  return $form;
}


/*
 * Implements hook_form_submit().
 */
function megamenu_framework_menuitems_admin_form_submit($form, $form_state) {
  foreach ($form_state['values']['categories'] as $category) {
    $query = db_update('megamenu_framework')->fields(array(
        'weight' => $category['weight'],
      ))->condition('rid', $category['rid'])->execute();
  }
  drupal_set_message('The order has been saved', 'status');
  megamenu_framework_menuitems_flush();
}


/*
 * Implements hook_form().
 */
function megamenu_framework_menuitems_add_form($form, &$form_state) {
  $settings = variable_get('megamenu_framework_event_type');
  drupal_add_js(drupal_get_path('module', 'megamenu_framework') . '/inc/megamenu_framework_admin.js', array('type' => 'file', 'preprocess' => FALSE));

  $form['back_link'] = array(
    '#markup' => '<p style="text-align:right">' . l(t('Back to list'), MEGAMENU_FRAMEWORK_ADMINPATH) . '</p>',
  );
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Category'),
    '#required' => TRUE,
    '#description' => t('This is the text that will appear in the main menu button. Plain text only, no tags.'),
  );
  $form['machine_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Machine Name'),
    '#required' => TRUE,
    '#description' => t('Must be unique, alphanumeric characters, underscores and dashes only.'),
  );
  $form['columns'] = array(
    '#type' => 'textfield',
    '#title' => t('Columns'),
    '#default_value' => 3,
    '#required' => TRUE,
    '#description' => t('The number of columns in the pulldown.'),
  );
  $event_type = variable_get('megamenu_framework_event_type');
  if(variable_get('megamenu_framework_event_type') == 'hover' && variable_get('megamenu_framework_links') == 1) {
    $form['url'] = array(
      '#type' => 'textfield',
      '#title' => t('URL'),
      '#required' => FALSE,
      '#description' => t('An optional url for the main menu items, if the event type is set to hover. Accepts absolute, relative and Drupal machine paths. If left blank, the item will still be wrapped in a anchor tag but with an href of "#" and a class of "no-link"'),
    );
    $form['alt'] = array(
      '#type' => 'textfield',
      '#title' => t('Alt tag'),
      '#required' => FALSE,
      '#description' => t('The ALT tag for the optional url. Will also override the wrapper div\'s "title" attribute. If left blank the main title will be used for the ALT tag.'),
    );
    $form['newwin'] = array(
      '#type' => 'checkbox',
      '#title' => t('Open URL in a new window.'),
      '#required' => FALSE,
    );
  }
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add Category'),
  );
  return $form;
}



/*
 * Implements hook_form_validate().
 */
function megamenu_framework_menuitems_add_form_validate($form, &$form_state) {
  $resultTitle = db_select('megamenu_framework', 'mf')
    ->fields('mf', array('rid'))
    ->condition('title', $form_state['values']['title'])
    ->countQuery()
    ->execute()->fetchField();

  if ($resultTitle) {
    form_set_error('title', t('This category title has already been used'));
  }

  $resultMachinename = db_select('megamenu_framework', 'mf')
    ->fields('mf', array('rid'))
    ->condition('machine_name', $form_state['values']['machine_name'])
    ->countQuery()
    ->execute()->fetchField();

  if ($resultMachinename) {
    form_set_error('machine_name', t('This machine name has already been used. Machine names must be unique.'));
  }
}


/*
 * Implements hook_form_submit().
 */
function megamenu_framework_menuitems_add_form_submit($form, &$form_state) {
  if (isset($form_state['values']['url'])) {
    $data = array(
      'title' => $form_state['values']['title'],
      'machine_name' => $form_state['values']['machine_name'],
      'columns' => $form_state['values']['columns'],
      'url' => $form_state['values']['url'],
      'alt' => $form_state['values']['alt'],
      'newwin' => $form_state['values']['newwin'],
      'weight' => 0,
    );
  }
  else {
    $data = array(
      'title' => $form_state['values']['title'],
      'machine_name' => $form_state['values']['machine_name'],
      'columns' => $form_state['values']['columns'],
      'weight' => 0,
    );
  }
  db_insert('megamenu_framework')->fields($data)->execute();

  drupal_set_message(t('The category "@category" has been added.', array('@category' => $form_state['values']['title'])), 'status');
  watchdog('megamenu_framework', 'The category "@category" was added.', array('@category' => $form_state['values']['title']), WATCHDOG_INFO);
  megamenu_framework_menuitems_flush();
}



/*
 * Implements hook_form().
 */
function megamenu_framework_menuitems_edit_form($form, &$form_state, $rid) {
  drupal_add_js(drupal_get_path('module', 'megamenu_framework') . '/inc/megamenu_framework_admin.js', array('type' => 'file', 'preprocess' => FALSE));

  $result = db_select('megamenu_framework', 'mf')->fields('mf', array('rid', 'title', 'machine_name', 'columns', 'url', 'alt', 'newwin'))->condition('rid', $rid)->execute()->fetchAll();

  $form['back_link'] = array(
    '#markup' => '<p style="text-align:right">' . l(t('Back to list'), MEGAMENU_FRAMEWORK_ADMINPATH) . '</p>',
  );
  $form['rid'] = array(
    '#type' => 'hidden',
    '#value' => $rid,
  );
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#required' => TRUE,
    '#default_value' => $result[0]->title,
    '#description' => t('This is the text that will appear in the main menu button. Plain text only, no tags.'),
  );
  $form['machine_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Machine Name'),
    '#default_value' => $result[0]->machine_name,
    '#required' => TRUE,
    '#description' => t('Must be unique, alphanumeric characters, underscores and dashes only.<br />NOTE: Changing the machine name will cause of this category\'s blocks to be disabled and they\'ll have to be re-assigned.'),
  );
  $form['columns'] = array(
    '#type' => 'textfield',
    '#title' => t('Columns'),
    '#required' => TRUE,
    '#default_value' => $result[0]->columns,
    '#description' => t('The number of columns in the pulldown. NOTE: Reducing the number of columns will cause the block assigned to the removed columns to be disabled.'),
  );

  if(variable_get('megamenu_framework_event_type') == 'hover' && variable_get('megamenu_framework_links') == 1) {
    $form['url'] = array(
      '#type' => 'textfield',
      '#title' => t('URL'),
      '#default_value' => $result[0]->url,
      '#required' => FALSE,
      '#description' => t('An optional url for the main menu items, if the event type is set to hover. Accepts absolute, relative and Drupal machine paths. If left blank, the item will still be wrapped in a anchor tag but with an href of "#" and a class of "no-link"'),
    );
    $form['alt'] = array(
      '#type' => 'textfield',
      '#title' => t('Alt tag'),
      '#default_value' => $result[0]->alt,
      '#required' => FALSE,
      '#description' => t('The ALT tag for the optional url. Will also override the wrapper div\'s "title" attribute. If left blank the main title will be used for the ALT tag.'),
    );
    $form['newwin'] = array(
      '#type' => 'checkbox',
      '#title' => t('Open URL in a new window.'),
      '#default_value' => $result[0]->newwin,
      '#required' => FALSE,
    );
  }

  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Update Category'));
  return $form;
}


/*
 * Implements hook_form_validate().
 */
function megamenu_framework_menuitems_edit_form_validate($form, &$form_state) {
  $resultTitle = db_select('megamenu_framework', 'mf')
    ->fields('mf', array('rid'))
    ->condition('title', $form_state['values']['title'])
    ->condition('rid', $form_state['values']['rid'], '<>')
    ->countQuery()
    ->execute()->fetchField();

  if ($resultTitle) {
    form_set_error('title', t('This category title has already been saved'));
  }

  $resultMachinename = db_select('megamenu_framework', 'mf')
    ->fields('mf', array('rid'))->condition('machine_name', $form_state['values']['machine_name'])
    ->condition('rid', $form_state['values']['rid'], '<>')
    ->countQuery()
    ->execute()->fetchField();

  if ($resultMachinename) {
    form_set_error('machine_name', t('This machine name has already been used. Machine names must be unique.'));
  }
}


/*
 * Implements hook_form_submit().
 */
function megamenu_framework_menuitems_edit_form_submit($form, &$form_state) {
  if (isset($form_state['values']['url'])) {
    $data = array(
      'title' => $form_state['values']['title'],
      'machine_name' => $form_state['values']['machine_name'],
      'columns' => $form_state['values']['columns'],
      'url' => $form_state['values']['url'],
      'alt' => $form_state['values']['alt'],
      'newwin' => $form_state['values']['newwin'],
    );
  }
  else {
    $data = array(
      'title' => $form_state['values']['title'],
      'machine_name' => $form_state['values']['machine_name'],
      'columns' => $form_state['values']['columns'],
    );
  }
  db_update('megamenu_framework')->fields($data)->condition('rid', $form_state['values']['rid'])->execute();

  drupal_set_message(t('The category "@category" has been updated.', array('@category' => $form_state['values']['title'])), 'status');
  watchdog('megamenu_framework', 'The category "@category" was updated', array('@path' => $form_state['values']['title']), WATCHDOG_INFO);
  megamenu_framework_menuitems_flush();
}


/*
 * Implements hook_form().
 */
function megamenu_framework_menuitems_delete_form($node, &$form_state, $rid) {
  $result = db_select('megamenu_framework', 'mf')->fields('mf', array('rid', 'title'))->condition('rid', $rid)->execute()->fetchAll();

  $form['back_link'] = array(
    '#markup' => '<p style="text-align:right">' . l(t('Back to list'), MEGAMENU_FRAMEWORK_ADMINPATH) . '</p>',
  );
  $form['markup'] = array(
    '#markup' => '<p>' . t('Delete the category "@title"', array('@title' => $result[0]->title)) . '. All it\'s block will be disabled.</p><p>This cannot be undone.</p>',
  );
  $form['rid'] = array(
    '#type' => 'hidden',
    '#value' => $result[0]->rid,
  );
  $form['title'] = array(
    '#type' => 'hidden',
    '#value' => $result[0]->title,
  );
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Delete Category'));
  return $form;
}


/*
 * Implements hook_form_submit().
 */
function megamenu_framework_menuitems_delete_form_submit($form, &$form_state) {
  // Delete the path
  db_delete('megamenu_framework')->condition('rid', $form_state['values']['rid'])->execute();
  drupal_set_message(t('The category "@title" has been deleted.', array('@title' => $form_state['values']['title'])), 'status');
  watchdog('megamenu_framework', 'The path "@title" was deleted', array('@title' => $form_state['values']['title']), WATCHDOG_INFO);
  megamenu_framework_menuitems_flush();
}


/*
 * Implements hook_form().
 */
function megamenu_framework_stop_propegation_obj_form($form, &$form_state) {
  $form['help'] = array(
    '#type' => 'markup',
    '#markup' => t('<h2>Exempt Page Objects from Megamenu Click Events</h2><p>This allows you to specify objects with jquery identifiers that will not fire the jquery "click" events.<br>The default behavior is that mouse clicks anywhere but on the main menu items will close any open popup. But what if for example you put a form in the megamenu? You don\'t want the pulldown to close when someone clicks on the input field. So enter that object\'s jquery identifier string here and it will be exempted from the behavior</p>'),
  );
  $form['jquery'] = array(
    '#type' => 'textarea',
    '#title' => t('jQuery Identifiers'),
    '#default_value' => variable_get('megamenu_framework_stop_propagaton_objects', ''),
    '#description' => t('One complete jQuery object identification string per line. No quotes.'),
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Identifiers'),
  );
  return $form;
}


/*
 * Implements hook_form_submit().
 */
function megamenu_framework_stop_propegation_obj_form_submit($form, &$form_state) {
  variable_set('megamenu_framework_stop_propagaton_objects', $form_state['values']['jquery']);
  drupal_set_message(t('Your exemptions have been saved.'));
}

/*
 * Implements hook_form().
 */
function megamenu_framework_settings_export_form($form, &$form_state) {
  $variables = array();
  $available_variables = array(
    'megamenu_framework_stop_propagaton_objects',
    'megamenu_framework_event_type',
    'megamenu_framework_placement',
    'megamenu_framework_links'
  );
  foreach ($available_variables as $av) {
    $var = variable_get($av, 0);
    if ($var) {
      $variables[] = array('name' => $av, 'value' => $var);
    }
  }
  if (count($variables)) {
    $output['variables'] = $variables;
  }

  $result = db_query("SELECT * FROM {megamenu_framework}");
  if ($result) {
    while ($row = $result->fetchAssoc()) {
      $data[] = $row;
    }
  }
  if (count($data)) {
    $output['db'] = $data;
  }
  $export_data = serialize($output);
  $form['help'] = array(
    '#type' => 'markup',
    '#markup' => t('<h2>Export Megamenu Frameworks Settings</h2><p>This only includes the settings that are defined on this module\'s admin page. Blocks and block assignments have to be exported using <a href="https://www.drupal.org/project/features_extra" target="_blank">Features</a> and <a href="https://www.drupal.org/project/context" target="_blank">Context</a>.</p>'),
  );
  $form['export_data'] = array(
    '#type' => 'textarea',
    '#description' => t('Copy this code and paste it into the matching import field.'),
    '#value' => $export_data,
  );
  return $form;
}

/*
 * Implements hook_form().
 */
function megamenu_framework_settings_import_form($form, &$form_state) {
  $form['help'] = array(
    '#type' => 'markup',
    '#markup' => t('<h2>Import Megamenu Frameworks Settings</h2><p><strong>Caution: This will overwrite any settings you currently have.</strong> This only includes the settings that are defined on this module\'s admin page. Blocks and block assignments have to be imported using <a href="https://www.drupal.org/project/features_extra" target="_blank">Features</a> and <a href="https://www.drupal.org/project/context" target="_blank">Context</a>.</p>'),
  );
  $form['import_data'] = array(
    '#type' => 'textarea',
    '#description' => t('Past the code from the matching export field.'),
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Import Settings'),
  );
  return $form;
}

/*
 * Implements hook_form_submit().
 */
function megamenu_framework_settings_import_form_submit($form, &$form_state) {
  // Wipe all pre-existing data
  $available_variables = array(
    'megamenu_framework_stop_propagaton_objects',
    'megamenu_framework_event_type',
    'megamenu_framework_placement',
    'megamenu_framework_links'
  );
  foreach ($available_variables as $av) {
    $var = variable_del($av);
  }
  $result = db_query("SELECT rid FROM {megamenu_framework}");
  if ($result) {
    while ($row = $result->fetchAssoc()) {
      db_delete('megamenu_framework')
        ->condition('rid', $row['rid'])
        ->execute();
    }
  }
  // Import the new data
  $data = unserialize($form_state['values']['import_data']);
  if (isset($data['variables'][0])) {
    foreach($data['variables'] as $var) {
      variable_set($var['name'], $var['value']);
    }
  }
  if (isset($data['db'][0])) {
    foreach($data['db'] as $row) {
      db_insert('megamenu_framework')
        ->fields(array(
          'rid' => $row['rid'],
          'machine_name' => $row['machine_name'],
          'title' => $row['title'],
          'columns' => $row['columns'],
          'weight' => $row['weight'],
          'url' => $row['url'],
          'alt' => $row['alt'],
          'newwin' => $row['newwin'],
        ))
        ->execute();
    }
  }
  // Flushes the cache when categories (theme regions) are added, deleted or modified
  system_rebuild_theme_data(); // To refresh the Blocks UI page
  cache_clear_all(MEGAMENU_FRAMEWORK_SAVED_REGIONS_CACHEKEY, 'cache');
}


