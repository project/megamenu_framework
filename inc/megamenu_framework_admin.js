var Megamenu_framework_persistant_state = {index:-1, active:0};
(function ($) {
    Drupal.behaviors.megamenu_framework_admin = {
        attach: function (context, settings) {
            // BEGIN document ready

            // Copy the title value to the machine name field, removing special characters
            $('#edit-title').blur(function() {
                if ($('#edit-machine-name').val() == '') {
                    $('#edit-machine-name').val(removeSpecials($('#edit-title').val()));
                }
            });

            // Show or hide the click exemptions form depending on the selected trigger event.
            $('#edit-event-type input:checked').each(function() {
                if ($(this).val() == 'hover') {
                    clickExemptions(0);
                    linksCheckbox(1);
                }
                else {
                    clickExemptions(1);
                    linksCheckbox(0);
                }
            });

            $( "#edit-event-type input" ).change(function() {
                if ($(this).val() == 'hover') {
                    clickExemptions(0);
                    linksCheckbox(1);
                }
                else {
                    clickExemptions(1);
                    linksCheckbox(0);
                }
            });

            function clickExemptions(bool) {
                if (bool) {
                    // Show the field
                    $('#megamenu-framework-stop-propegation-obj-form').show();
                }
                else {
                    // Hide the field
                    $('#megamenu-framework-stop-propegation-obj-form').hide();
                }
            }
          function linksCheckbox(bool) {
            if (bool) {
              // Show the field
              $('.form-type-checkbox.form-item-links').show();
            }
            else {
              // Hide the field
              $('.form-type-checkbox.form-item-links').hide();
            }
          }

            /**
             * @param str
             * @returns cleaned string
             */
            function removeSpecials(str) {
                str =  str.replace(/[^a-zA-Z0-9 ]/g, "");
                str = str.replace(/ /g, '_');
                return str.toLowerCase();
            }
            // END document ready
        }
    };
})(jQuery);
